﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Node {
    char symbol;
    int freq;
    struct Node* left, * right;
} Node;

Node* newNode(char symbol, int freq, Node* left, Node* right) {
    Node* node = (Node*)malloc(sizeof(Node));
    node->symbol = symbol;
    node->freq = freq;
    node->left = left;
    node->right = right;
    return node;
}

void printCodes(Node* root, int* code, int top) {
    if (root->left) {
        code[top] = 0;
        printCodes(root->left, code, top + 1);
    }

    if (root->right) {
        code[top] = 1;
        printCodes(root->right, code, top + 1);
    }

    if (!(root->left) && !(root->right)) {
        printf("%c: ", root->symbol);
        for (int i = 0; i < top; ++i)
            printf("%d", code[i]);
        printf("\n");
    }
}

void shannonFano(char* data) {
    int freq[256] = { 0 };
    for (int i = 0; data[i] != '\0'; ++i)
        ++freq[data[i]];

    Node* nodes[256];
    int size = 0;
    for (int i = 0; i < 256; ++i) {
        if (freq[i] > 0) {
            nodes[size++] = newNode(i, freq[i], NULL, NULL);
        }
    }

    while (size > 1) {
        for (int i = 0; i < size; ++i) {
            for (int j = i + 1; j < size; ++j) {
                if (nodes[i]->freq < nodes[j]->freq) {
                    Node* temp = nodes[i];
                    nodes[i] = nodes[j];
                    nodes[j] = temp;
                }
            }
        }

        Node* left = nodes[--size];
        Node* right = nodes[--size];
        nodes[size++] = newNode('\0', left->freq + right->freq, left, right);
    }

    int code[256];
    printCodes(nodes[0], code, 0);
}

int main() {
    char data[] = "hello world";
    shannonFano(data);
    return 0;
}
